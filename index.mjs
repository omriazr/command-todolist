import { promises } from 'dns';
import fs from 'fs/promises';
import readline from 'readline';

const uniqeID = () => Math.random().toString(36).substring(2);
const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

function prompt(question) {
    return new Promise((resolve, error) => {
        rl.question(question, answer => {
            rl.close()
            resolve(answer)
        });
    })
}
// const commandMapper = {
//     1: {
//         title:'enter the task',
//         executer: create
//     }
//     2: {
//         title:'press id note that you want to remove',
//          executer: remove
//     }
//      3: {
//         title:'press id note that you want to change mod',
//         executer: change_mod
//      }
// }

async function init() {
    // read todo.json 
    let data = await fs.readFile(`./todolist.json`, 'utf-8');
    data = JSON.parse(data);
    // creat arrNots and move there data 
    let arrNots = data || [];
    // call action fun and send arrNots 
    action(arrNots);
}

async function action(arrNots) {
    // show nots 
    console.log(arrNots)
    // show options of action
    console.log("actions : \n 1- create \n 2- remove \n 3-change_mod")
    // get the choosen options fun and call her

    rl.question('press number of What you want to do ? ', (commandType) => {
        console.log(`Oh, so your want to do  ${commandType}`);
        console.log("show you again the nots ", arrNots)

        // rl.question(ommandMapper[commandType].title ,(answer) => commandMapper[commandType].executer(arrNots,answer));
        let isright = false;

        switch (commandType) {
            case "1":
                rl.question('press value of the note', (value) => {
                    create(arrNots, value)
                });
                break;
            case "2":
                // while (!isright) {
                rl.question('press id note that you want to remove', (id) => {
                    console.log(id);
                    if (arrNots.find(note => note.idNote === id)) { remove(arrNots, id) }
                    else {
                        console.log("the id didnt exist, try again");
                    }
                });
                // }
                break;
            case "3":
                // while (!isright) {
                rl.question('press id note that you want to change mod to complete', (id) => {
                    if (arrNots.find(note => note.idNote === id)) { complete(arrNots, id) }
                    else {
                        console.log("the id didnt exist, try again");
                    }
                });
                // }
                break;
            default:
                console.log(`error, enter new input`);
                action(arrNots);
        }
    });

}

// async function wait (str , cb, ){
//     return new promises((res,rej) => 
//     rl.question('press id note that you want to remove', (id) => {
//         if (arrNots.find(note => note.idNote === Number(id))) { remove(arrNots, id) }
//         else {
//             console.log("the id didnt exist, try again");
//         }
//     }))
// }

function create(arrNots, value) {
    console.log("im in create")
    // creat object 
    const newNote = {
        "idNote": uniqeID(),
        "value": value,
        "complete": false

    }
    console.log(newNote)
    // add object to arrNots
    arrNots.push(newNote);
    // call save fun 
    save(arrNots);
}

function remove(arrNots, idToRemove) {
    console.log("im in remove")
    // remove obj idNote from arrNots

    arrNots = arrNots.filter((item) => item.idNote !== idToRemove)
    console.log(arrNots)
    // call save fun
    save(arrNots);
}

function complete(arrNots, idToChange) {
    console.log("im in complete")
    // change mod complete 
    const noteObj = arrNots.find((item) => item.idNote === Number(idToChange))
    noteObj.complete = noteObj.complete ? false : true;
    console.log(noteObj);
    // call save fun 
    save(arrNots)
}
async function save(arrNots) {
    // save arrNots to todo.json 
    await fs.writeFile(`./todolist.json`, JSON.stringify(arrNots, null, 2), 'utf-8')
    // call action fun
    action(arrNots);
}
init();