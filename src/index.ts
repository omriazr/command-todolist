import {argsParse} from './utils/cli.utils.js'
import {execute} from './controller/todo.controller.js'
import { validateArgs } from './controller/validators.js';

 async function init(){

    const options =argsParse(process.argv);
    console.log(options);
    const argsValid = validateArgs(options)
    if(argsValid){
     await execute(options);
    }
}
init();