import { uniqeID } from "../utils/string.utils";
import { expect } from "chai";

describe(`String utils tests`, () => {

    it(`should be a function`, () => {
        expect(uniqeID).to.be.a("function");
        expect(uniqeID).to.be.instanceOf(Function);
    });
   
    it(`should return uniqeID with 5 chars`, async () => {
        console.log(uniqeID().length)
         expect(uniqeID().length).to.equal(5)
     });
});