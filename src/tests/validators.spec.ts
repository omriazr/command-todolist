import { validateLength, validateCommandExist } from "../controller/validators";
import { errorLog } from "../view/cli.output";
import { expect } from "chai";

describe(`Validators utils tests`, () => {
    context('tests for validateLength function', () => {
        it(`should be a function`, () => {
            expect(validateLength).to.be.a("function");
            expect(validateLength).to.be.instanceOf(Function);
        });
       
        it(`Call the funcion without params, should ,should return false`, async () => {
            
             expect(validateLength([])).to.equal(false);
         });
    
         it(`Call the funcion with 1 param, should ,should return true`, async () => {
            
            expect(validateLength(['create'])).to.equal(true);
        });
    
        it(`Call the funcion with 2 params, should ,should return true`, async () => {
            
            expect(validateLength(['create','-b'])).to.equal(true);
        });
    
        it(`Call the funcion with 3 params, should ,should return true`, async () => {
            
            expect(validateLength(['create','-b','buy corn'])).to.equal(true);
        });
    
        it(`Call the funcion with 4 params, should ,should return false`, async () => {
            
            expect(validateLength(['create','-b','buy corn','buy milk'])).to.equal(false);
        });

    })

    context('tests for validateCommandExist function', () => {
        it(`should be a function`, () => {
            expect(validateCommandExist).to.be.a("function");
            expect(validateCommandExist).to.be.instanceOf(Function);
        });

        it(`Call the funcion with 'create' as param, should return true`, () => {
            expect(validateCommandExist(['create'])).to.equal(true);
        });

        it(`Call the funcion with 'update' as param, should return true`, () => {
            expect(validateCommandExist(['update'])).to.equal(true);
        });

        it(`Call the funcion with 'read' as param, should return true`, () => {
            expect(validateCommandExist(['read'])).to.equal(true);
        });

        it(`Call the funcion with 'remove' as param, should return true`, () => {
            expect(validateCommandExist(['remove'])).to.equal(true);
        });

        it(`Call the funcion with 'help' as param, should return true`, () => {
            expect(validateCommandExist(['help'])).to.equal(true);
        });

        it(`Call the funcion with 'add' as param, should return false`, () => {
            expect(validateCommandExist(['add'])).to.equal(false);
        });

        it(`Call the funcion with 'delete' as param, should return false`, () => {
            expect(validateCommandExist(['delete'])).to.equal(false);
        });
    })

    // it(`Call the funcion with 4 params, should ,should throw error`, async () => {
    //     try{
    //         validateLength(['create','-b','buy corn','buy milk'])

    //     } catch(err){
    //         expect(err.message).to.equal(true);

    //     }
    // });
});