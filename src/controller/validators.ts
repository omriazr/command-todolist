import { errorLog } from "../view/cli.output.js";
import { commands } from "./schema.js";

export function validateLength(args: string[]):boolean{
    if(args.length > 3){
        errorLog("the app cant get more then 3 args");
        return false;
    }
    if(args.length == 0) {
        errorLog("the app need to get command to execute");
        return false;
    }
    return true;
}

export function validateCommandExist(args: string[]): boolean {
    if (args[0] in commands) {
        return true
    }
    errorLog(`command ${args[0]} didnt exist in app schema\n try todos help`);
    return false;
}


function validateParmsExist(args: string[]): boolean {
    if(args[1]=== undefined) return true;
    const existParm = commands[args[0]].some((parm) => parm === args[1])
    if (existParm) {
        return true;
    }
    errorLog(`param ${args[1]} didnt exist in ${args[0]} command\n try todos help`);
    return false;
}


export function validateArgs(args: string[]): boolean {
    if (validateLength(args) && 
    validateCommandExist(args) && 
    validateParmsExist(args)) {
        return true;
    }
    else return false;
}