import { Note } from "../types/todo.interface.js";
import { get_arrNote, save } from "../db/data.access.js";
import { errorLog, show, usage } from "../view/cli.output.js";
import { prompt } from "../utils/rl.async.js";
import { uniqeID } from "../utils/string.utils.js";

export async function execute(option: string[]) {
  let arrNots = await get_arrNote();

  switch (option[0]) {
    case "create":
      if (!option[2])
        option[2] = await prompt("press value note that you want to add");
      create(arrNots, option[2]);
      break;
    case "read":
      read(arrNots, option[1]);
      break;
    case "update":
      if (!option[2])
        option[2] = await prompt("press id note that you want to update");
      update(arrNots, option[2]);
      break;
    case "remove":
      remove(arrNots, option[1], option[2]);
      break;
    case "help":
      usage();
      break;
  }
  return;
}
function read(arrNots: Note[], option: string) {
  console.log(option);
  switch (option) {
    case "--all":
    case "-a":
      show(arrNots);
      break;
    case "--uncomplete":
    case "-uc":
      show(arrNots.filter((item) => item.complete === false));
      break;
    case "--complete":
    case "-c":
      show(arrNots.filter((item) => item.complete === true));
      break;
    default:
      show(arrNots);
      console.log("default");
      break;
  }
  return;
}

async function create(arrNots: Note[], value: string) {
  let tasksArr: string[] = [];
  tasksArr = value.split(" | ");

  for (const task of tasksArr) {
    const newNote: Note = {
      idNote: uniqeID(),
      value: task,
      complete: false,
    };

    // add Note to arrNots
    arrNots.push(newNote);
  }

  // call save fun
  save(arrNots);
  show(arrNots);
  return arrNots;
}

async function update(arrNots: Note[], idToUpdate: string) {
  const noteObj: undefined | Note = arrNots.find(
    (item: Note) => item.idNote === idToUpdate
  );
  if (noteObj === undefined) {
    errorLog("invalid id ,check the ids that exist with read --all command");
  } else {
    // change mod
    noteObj.complete = noteObj.complete ? false : true;
    // call save fun
    save(arrNots);
    show(arrNots);
  }
}

async function remove(arrNots: Note[], param: string, idToRemove: string) {
  if ((param = "-id")) {
    await removeById(arrNots, idToRemove);
  } else {
    removeAllComplete(arrNots);
  }
}

async function removeById(arrNots: Note[], idToRemove: string) {
  if (idToRemove === undefined) {
    idToRemove = await prompt("press id note that you want to remove");
  }

  const noteObj: undefined | Note = arrNots.find(
    (item: Note) => item.idNote === idToRemove
  );
  if (noteObj === undefined) {
    errorLog("invalid id ,check the ids that exist with show all command");
  } else {
    // remove obj idNote from arrNots
    arrNots = arrNots.filter((item) => item.idNote !== idToRemove);
    // call save fun
    save(arrNots);
    show(arrNots);
  }
}

function removeAllComplete(arrNots: Note[]) {
  //remove all complete task
  arrNots = arrNots.filter((item) => item.complete === false);
  //call save fun
  save(arrNots);
  show(arrNots);
}
