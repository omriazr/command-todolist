import { errorLog } from "../view/cli.output.js";

export function argsParse(argv:string[]) : string[] {
    console.log(argv);
    let myArgs: string[] = argv.slice(2);
    if(myArgs.length == 2){
        myArgs = [myArgs[0], ...myArgs[1].split("=")]
    }
    return myArgs;
}