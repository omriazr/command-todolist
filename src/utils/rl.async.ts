import readline from 'readline';


function create_rl(){
const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});
return rl
}

export function prompt(question: string): Promise<string> {
    return new Promise((resolve, error) => {
        const rl = create_rl();
        rl.question(question, answer => {
            rl.close()
            resolve(answer)
        });
    })
}