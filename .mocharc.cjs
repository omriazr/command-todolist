module.exports = 
{
  "extension": [
      "ts"
  ],
  "spec": "./src/tests/**/*.spec.ts",
  "node-option": [
      "experimental-specifier-resolution=node",
      "loader=ts-node/esm"
  ],
  "timeout": "5000"
};